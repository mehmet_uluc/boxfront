<?php include "shared/main_bar.php" ?>
<div class="dashboard login">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50 ">
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-12">
                                    <h3 class="tac mb30 secondary-color">boxofus Gizlilik Ve Güvenlik Politikası</h3>
                                    <div class="ff-mr">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet dolor
                                            est hic id illo iure natus placeat voluptatum. At consequatur dicta ea ex
                                            odio quibusdam sint tempora vitae voluptates!
                                        </p>
                                        <p>Ab atque consectetur distinctio ducimus est excepturi fugiat, minima nam
                                            natus nisi nulla numquam odit officia officiis pariatur praesentium quidem
                                            quisquam quod quos ratione repellat ullam ut veritatis voluptate voluptates.
                                        </p>
                                        <p>Ab beatae consectetur cumque distinctio, et eveniet facere facilis in ipsa
                                            labore magni natus nesciunt nobis odio pariatur quae qui quo rem, saepe
                                            voluptatibus. Hic sapiente sequi suscipit vel voluptate?
                                        </p>
                                        <p>Adipisci dolore est illum incidunt necessitatibus neque sunt ullam ut vel
                                            voluptas? Commodi consequuntur dolorum non omnis quasi. Aspernatur at
                                            consequuntur dignissimos dolore eum facilis iusto molestias porro totam
                                            voluptatum!
                                        </p>
                                        <p>Assumenda atque blanditiis debitis, excepturi maxime recusandae tenetur
                                            voluptatem. Aspernatur beatae cupiditate esse quo similique soluta tempore
                                            temporibus! Dicta esse expedita itaque iure nam nesciunt nihil porro quia,
                                            similique ullam?
                                        </p>
                                        <p>Adipisci aliquam aliquid aspernatur aut blanditiis deserunt doloremque
                                            doloribus dolorum, esse eveniet expedita id impedit labore, laudantium,
                                            magnam mollitia natus nemo neque perspiciatis quos reprehenderit sapiente
                                            suscipit velit. Maiores, quis.
                                        </p>
                                        <p>A beatae dolore fugiat id ipsa magni maxime nobis quae, repellendus? Ad cum
                                            dolorem error illum laudantium molestiae nemo nostrum nulla possimus,
                                            praesentium quisquam quo recusandae, repellat sint, sunt. Ab!
                                        </p>
                                        <p>Amet asperiores aspernatur corporis cupiditate facilis illum labore magni
                                            molestias, nemo, repellendus, sequi tempora vel voluptatem? Accusamus aut
                                            culpa error eveniet exercitationem impedit laborum necessitatibus quia
                                            sapiente similique? Doloribus, laboriosam.
                                        </p>
                                    </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.body.className += " hidden-footer";
</script>
