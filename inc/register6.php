<section id="register" class="no-padding">
    <a href="" class="checkout-logo">box of us</a>
    <div class="checkout-step-wrapper">
        <ul class="checkout-step">
            <li><a href=" " class="success"><span><i class="fa fa-check-circle"></i></span>Kayıt Ol / Üye Girişi</a></li>
            <li><a href=" " class="success"><span><i class="fa fa-check-circle"></i></span>Abonelik Kutusu</a></li>
            <li><a href=" " class="success"><span><i class="fa fa-check-circle"></i></span>Adres Bilgileri</a></li>
            <li><a href=" " class="active"><span>4</span>Ödeme</a></li>
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50">
                    <div class="content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="tac mb40">Kredi Kartı Bilgileri</h2>
                            </div>
                            <div class="col-md-5 tac mb40">
                                <img src="assets/img/checkout-box.jpg" class="box" alt="box of us">
                               <ul class="invoice">

                                   <li>
                                       <span>1 Adet boxofus aboneliği</span>
                                       <span class="fr">100 TL</span>
                                   </li>
                                   <li>
                                       <span>ilk 3 ay promosyon indirimi</span>
                                       <span class="fr">-10 TL</span>
                                   </li>
                                   <li>
                                       <span>Kargo</span>
                                       <span class="fr">Ücretsiz</span>
                                   </li>
                                   <li>
                                       <span>Aylık ödenilecek tutar (vergiler dahil)</span>
                                       <span class="fr secondary-color">90 TL</span>
                                   </li>
                                   <li>
                                       <span class="lh40">Kuponunuz var mı?</span>
                                       <input type="text" class="fr btn-md discount-code" placeholder="Kodu Yapıştırın">
                                   </li>
                               </ul>
                            </div>
                            <div class="col-md-1 mb40"> &nbsp;</div>
                            <div class="col-md-6">
                                <form id="register-form" class="form" action="" novalidate="novalidate" method="post">
                                    <ul class="form-wrapper">
                                        <li>
                                            <input type="text" id="name-surname" name="name-surname" placeholder="Ad Soyad" minlength="2" required>
                                        </li>
                                        <li>
                                            <input type="text" name="card-number" id="card-number" class="onlyNumeric" minlength="15" maxlength="16" placeholder="Kart numarası" required >
                                        </li>
                                        <li class="two-column clearfix">

                                            <div class="month-year two-column fl clearfix" >
                                                <input type="text" name="card-month" id="card-month" class="w20p fl onlyNumeric mr20" placeholder="Ay"  data-msg="" required data-rule-maxlength="2"  data-rule-minlength="2" data-rule-range="1,12">
                                                <input type="text" name="card-year" id="card-year" class="w50p fl onlyNumeric" placeholder="Yıl"  data-msg="" required maxlength="2">
                                            </div>

                                            <div class="cvv fr tar clearfix">
                                                <input type="text" name="cvv" class="onlyNumeric w100p" placeholder="CVV" required data-msg=""  maxlength="3">
                                            </div>

                                        </li>

                                        <li class="contract-row">
                                            <input name="contract" id="contract" type="checkbox" required data-msg="">
                                            <label for="contract">
                                                <a href=" ">Mesafeli satış sözleşmesini</a> ve
                                                <a href=" ">ön bilgilendirme formunu </a> okudum ve kabul ediyorum.
                                            </label>
                                        </li>
                                        <li class="tac mt40">
                                            <input type="submit" class="btn btn-secondary btn-lg btn-form" value="Abone Ol">
                                        </li>
                                        <li class="tac mt30">
                                            <img src="assets/img/guvenlifooter.png" alt="">
                                        </li>

                                    </ul>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="register-footer">
                                    <li>
                                        <i class="icon-free-shipping db"></i>
                                        <span> Bedava kargo</span>
                                    </li>
                                    <li>
                                        <i class="icon-cancel-account db"></i>
                                        <span> Dilediğinizde aboneliğinizi iptal etme</span>
                                    </li>
                                    <li>
                                        <i class="icon-happiness db"></i>
                                        <span>100% Müşteri memnuniyeti</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    document.body.className += " hidden-footer";
</script>