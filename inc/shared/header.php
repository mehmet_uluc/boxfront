<header>
    <nav class="nav">
        <a href=" " class="logo">
            <img src="assets/img/logo.png" alt="Box of us">
        </a>
        <ul class="menu hidden-sm hidden-xs">
            <li>
                <a href=" ">Box of us Nedir?</a>
            </li>
            <li>
                <a href=" ">Sıkça Sorulan Sorular</a>
            </li>
            <li>
                <a href=" ">Basında Biz</a>
            </li>
        </ul>

        <div class="user-zone-login hidden-sm hidden-xs">
            <a href="#" class="user-btn"> Yasemen  <i class="fa fa-sort-desc"></i></a>
            <ul class="user-menu">
                <li>
                    <a href="#"> Siparişlerim</a>
                </li>
                <li>
                    <a href="#"> Profilim </a>
                </li>
                <li>
                    <a href="#"> Parolam</a>
                </li>
                <li>
                    <a href="#"> Adreslerim </a>
                </li>
                <li>
                    <a href="#"> Çıkış Yap </a>
                </li>
            </ul>
        </div>

        <div class="user-zone hidden-sm hidden-xs" style="display: none">
            <a href=" " class="login"> Üye Girişi</a>
            <a href=" " class="btn btn-md btn-secondary"> Abone Ol</a>
        </div>



        <div class="mobile-btn" id="mobile-menu">
            <span class="top"></span>
            <span class="middle"></span>
            <span class="bottom"></span>
        </div>


        <div class="overlay" id="overlay">
            <nav class="overlay-menu">
                <ul>
                    <li><a href="#">Box of us nedir?</a></li>
                    <li><a href="#">Sıkça sorulan sorular</a></li>
                    <li><a href="#">Basında Biz</a></li>
                    <li class="btns">
                        <a href="#" class="btn btn-md2 btn-primary">Üye Girişi</a>
                        <a href="#" class="btn btn-md2 btn-secondary">Abone Ol</a>
                    </li>
                    <li class="social">
                        <a href=" " >
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href=" " >
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href=" " >
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a href=" " >
                            <i class="fa fa-pinterest"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

    </nav>
</header>


