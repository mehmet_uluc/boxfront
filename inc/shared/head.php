<head>
    <meta charset="UTF-8">
    <title>Box Of Us</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Hayatınıza heyecan katacak bir kutu box of us" />
    <meta name="keywords" content="Süpriz, box of us, dating" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="UTF-8">
    <meta name="Copyright" content="© Copyright 2017 Box Of Us" />
    <meta name="robots" content="noodp">
    <link rel="stylesheet" href="assets/css/plugins.css">
    <link rel="stylesheet" href="assets/css/header.css">
    <link rel="stylesheet" href="assets/css/fonts.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/sprite.css">
    <link rel="stylesheet" href="assets/css/jquery.mmenu.all.css">
    <link rel="stylesheet" href="assets/css/app.css">
</head>