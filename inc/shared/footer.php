<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="logos-wrapper">
                <ul class="logos">
                    <li>
                        Tech Crunch
                    </li>
                    <li>
                        Washington Post
                    </li>
                    <li>
                        The wall street Journal
                    </li>
                </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 tac nopadding">
                <div class="socia-wrapper">
                    <p>Bizi takip edin</p>
                    <ul>
                        <li>
                            <a href=" ">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href=" ">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href=" ">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href=" ">
                                <i class="fa fa-pinterest"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5 tal nopadding">
               <div class="footer-info">
                   <div class="logo">
                       <img src="assets/img/logo.png" alt="">
                   </div>
                   <p>
                       Aylık abonelik sistemiyle çalışan, ilişkiler/partner/evli
                       çiftlerin birbirleriyle daha kaliteli zaman geçirmelerini
                       sağlayan bir şirketdir.
                   </p>
               </div>
            </div>
            <div class="col-md-4 tac nopadding">
                <div class="email-wrapper">
                    <p>Email Bültenine Kayıt Olun</p>
                    <div class="display-table">
                        <input class="display-table-cell" type="text" placeholder="Email adresiniz">
                        <button class="display-table-cell" >Kayıt Ol</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
            <div class="col-md-12">
                <div class="footer-menu">
                    <ul>
                        <li><a href=" ">HAKKIMIZDA</a></li>
                        <li><a href=" ">DATE NIGHT BOX NEDİR?</a></li>
                        <li><a href=" ">SIKÇA SORULAN SORULAR</a></li>
                        <li><a href=" ">BASINDA BİZ</a></li>
                        <li><a href=" ">KULLANICI HAKLARI</a></li>
                        <li><a href=" ">GİZLİLİK SÖZLEŞMESİ</a></li>
                    </ul>
                </div>
                <div class="contact-us">
                    <ul>
                        <li>BİZE ULAŞIN :</li>
                        <li>merhaba@datenight.com</li>
                        <li class="white">+90-542-400-6348</li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
    </div>
</footer>
<script src="assets/js/jquery-1.11.3.js"></script>
<script src="assets/js/jquery.mmenu.all.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/jquery.plugin.min.js"></script>
<script src="assets/js/jquery.countdown.js"></script>
<script src="assets/js/jquery.countdown-tr.js"></script>
<script src="assets/js/jquery.validate.min.js"></script>
<script src="assets/js/messages_tr.js"></script>
<script src="assets/js/app.js"></script>
