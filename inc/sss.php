<?php include "shared/main_bar.php" ?>

<section id="sss-hero" class="hero">
    <div class="hero-wrapper display-table">
        <div class="hero-cell display-table-cell">
            <h1 class="secondary-color">Sıkça Sorulan Sorular</h1>
        </div>
    </div>
</section>

<section id="sss-content">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <p class="question">
                    Date Night’a abone olurken kutu yada aktivite
                    seçmek zorunda mıyız?
                </p>
                <p>
                    Evet. <br>
                    Date Night’a abonelikte iki secenek sunulur. Bu
                    seçeneklerden biri kutu, partnerinizle evde beraber
                    zaman geçirip, üretmeniz üretirken birbirinizi tanımanız
                    için hazırlanmış özel lezzetler ve oyunlar içerir.
                    Aktivite ise sosyalleşmenizi sağlamak amaçlı
                    düzenlediğimiz size özel tasarlanılmış etkinliklerden
                    ibarettir. Bu etkinliklerde ise hobilerinizi paylaşabileceğiniz
                    diğer çiftler ile tanışırsınız.
                </p>

                <p class="question">
                    Kutu'dan aktivite aboneliğine geçiş yada tam tersi
                    mümkün mü?
                </p>
                <p>
                    Evet. Aboneler istedikleri geçişi yapabilirler, Sitemize
                    giriş yaptıktan sonra abonelik türlerini diledikleri gibi
                    değiştirebilirler. Bu değişiklik bir sonraki ay'a yansıyacaktır.
                </p>

                <p class="question">
                    Oyunları kimler hazırlıyor?
                </p>
                <p>
                    Her ay kutu ve aktivite oyunlarımız uzman, konusunda
                    profosyonel bir ekip tarafından hazırlanıyor.
                </p>

            </div>
            <div class="col-md-5 col-md-push-1">
                <p class="question">
                    Hep aynı kutu ve aktiviteler mi var?
                </p>
                <p>
                    Hayır, her ay farklı konseptte kutu ve etkinlik düzenliyoruz.
                </p>

                <p class="question">
                    Kutular ne zaman gönderiliyor?
                </p>
                <p>
                    Kutular her ayın en geç 5'inde gönderilmiş oluyor.
                </p>

                <p class="question">
                    Aktiviteler ne zaman yapılıyor?
                </p>
                <p>
                    Aktiviteler her ay katılımcı sayısına göre haftada en az
                    1 kez olmak üzere 4 kez tekrarlanıyor.
                </p>

                <p class="question">
                    İstanbul dışında yaşıyorum, aktivite alabilir miyim?
                </p>
                <p>
                    Hayır, malesef. Sadece kutu aboneliği alabilirsiniz.
                    Şu anda istanbul dışında aktivite düzenliyemiyoruz.
                </p>

                <p class="question">
                    Kutum gelmedi, ne yapmaliyim?
                </p>
                <p>
                    Eger ayın 5'ine kadar kutunuzu almadıysanız mutlaka
                    <a href=" ">bize ulaşın.</a>
                </p>
            </div>
        </div>
    </div>
</section>