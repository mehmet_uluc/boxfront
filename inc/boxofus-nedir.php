<?php include "shared/main_bar.php" ?>

<section id="nedir-hero" class="hero">
    <div class="hero-wrapper display-table">
        <div class="hero-cell display-table-cell">
            <h1>Çünkü daha iyi bir ilişki için </h1>
            <div>
                <a href=" " class="btn btn-lg btn-primary mr40"> ABONE OL</a>
                <a href=" " class="btn btn-lg btn-white"> HEDİYE ET</a>
            </div>
        </div>
    </div>
</section>

<section id="nedir">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>box of us <br>
                    nedir?</h2>
            </div>
        </div>
    </div>
    <div class="container nedir-info">
        <div class="row">
            <div class="col-md-12">
                <p>
                    box of us partneri olan evli çiftler/hayat arkadaşları ve sevgililer icin ay'da sadece 1 geceyi beraber hem eğlenerek
                    hem öğrenerek geçirebilicekleri konsept kutular tasarlar. Bu gizemli <span class="secondary-color">SÜPRİZ</span> kutularda partnerinizle beraber üreteceğiniz
                    <span class="secondary-color">bir nesne </span>, bir <span class="secondary-color">yemek tarifi malzemeleri ile </span> ve ilişki uzmanları tarafından hazırlanılmış birbirinizi daha iyi tanımaya yönelten
                    <span class="secondary-color">oyunlar </span> bulunur.Her kutu gizemlidir, <span class="secondary-color">her ay farklı bir konseptde kutu yollanılır. </span>
                </p>
            </div>
        </div>
    </div>
    <div class="container nedir-img-wrapper">
        <div class="row">
            <div class="col-md-6 ">
                <img src="assets/img/nedir-box.png" >
            </div>
            <div class="col-md-6">
                <img src="assets/img/home-price.png" >
            </div>
        </div>
    </div>

</section>

<section id="how-does-it-work">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>
                    <i class="icon icon-logo dib"></i>
                    box of us nasıl işler?
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h2> 1- abone olursun</h2>
                <p>
                    Üye olursun, her ay SÜPRİZ
                    kutun kapıya gelir
                </p>
            </div>
            <div class="col-md-4">
                <h2> 2- box of us her ay kapina gelsin </h2>
                <p>
                    Her ay yollanilan kutular farkli konseptlere
                    sahiptir. Kutularin icinde oyun, yemek
                    ve testler bulunur.
                </p>
            </div>

            <div class="col-md-4">
                <h2> 3- eğlenin ve mutlu olun! </h2>
                <p>
                    Kutular partnerinizle kaliteli zaman
                    gecirmek uzerine  tasarlanilmistir.
                    MUTLU ve guzel bir geceye
                    hazir olun!
                </p>
            </div>
        </div>
    </div>
</section>

<section id="contents-of-box">
    <h2>box of us kutusunun icinde ne var?</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="col">
                    <i class="icon icon-what1 db">&nbsp;</i>
                    <h3>1 adet yemek tarifi
                        ve malzemeleri</h3>
                    <p>
                        ilişkinin en önemli kriteri zaman
                        geçirirken eğlenmek, eğlenirken
                        daha iyi tanıyabilmek.
                    </p>
                </div>
                <div class="col-name">
                    BİRBİRİNİZİ DAHA İYİ TANIYIN
                </div>
            </div>

            <div class="col-md-4">
                <div class="col">
                    <i class="icon icon-what2 db">&nbsp;</i>
                    <h3>Beraber üretebiliceğiniz
                        d.i.y. ürün malzemeleri</h3>
                    <p>
                        En son ne zaman partnerinizle bir şeyler
                        ürettiniz? box of us sizi tanıştığınız
                        ilk güne götürecek
                    </p>
                </div>
                <div class="col-name">
                    BİRBİRİNİZLE ZAMAN GEÇİRİN
                </div>
            </div>

            <div class="col-md-4">
                <div class="col">
                    <i class="icon icon-what3 db">&nbsp;</i>
                    <h3>Birbirinizi daha iyi tanımanız
                        için hazırlanmış oyun
                        konsepti
                    </h3>
                    <p>
                        box of us içinden çıkan deneyimlerle
                        rutinden uzak bir akşam geçiriceksiniz.
                        Deneyimler biribinizi tanımanız
                        için size bir sans vericektir.
                    </p>
                </div>
                <div class="col-name">
                    BİRBİRİNİZLE ZAMAN GEÇİRİN
                </div>
            </div>

        </div>
    </div>
</section>