<?php include "shared/dashboard_bar.php" ?>
<div class="dashboard">
    <div class=" container">
        <div class="row">
            <div class="col md-12">
                <div class="dashboard-inner content clearfix">
                   <h2 class="tac mt0">Ne Yapmak İstersiniz?</h2>
                    <div  class="what-do-you-want">
                        <div class="what-do-you-want-left">
                            <h4 class="mb30 header">boxofus hediye etmek istiyorum.</h4>
                            <p class="summary">
                                Birine verilebilecek en güzel hediye mükemmel
                                bir deneyimdir. Arkadaşlarınıza ve ailenize
                                doğum günü ve evlilik hediyesi olarak
                                boxofus’i hediye edebilirsiniz.
                            </p>
                            <img src="assets/img/gift-box.png" alt="" class="dib mb20"> <br>
                            <a href=" " class="btn btn-md btn-red-line">
                                HEDİYE ET
                            </a>
                        </div>
                        <div class="what-do-you-want-right">
                            <img src="assets/img/sprite/big-box.png" class="mb20" alt="">
                            <p>
                                <span class="secondary-color tdu mb20"> 2 adet </span>
                                aktif boxofus aboneliğiniz <br>
                                bulunmaktadır.
                            </p>
                            <a href=" " class="primary-color fz20 tdu">
                                Hesabım sayfasına <br>
                                gitmek istiyorum. >>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.body.className += " hidden-footer";
</script>
