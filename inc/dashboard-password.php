<?php include "shared/dashboard_bar.php" ?>
<div class="dashboard">
    <div class=" container">
        <div class="row">
            <div class="col md-12">
                <div class="dashboard-inner content clearfix">
                    <ul class="dashboard-menu">
                       <li>
                           <a href=" "> Siparişlerim </a>
                       </li>
                        <li>
                            <a href=" " > Profilim</a>
                        </li>
                        <li>
                            <a href=" " class="active"> Parolam</a>
                        </li>
                        <li>
                            <a href=" "> Adreslerim</a>
                        </li>
                    </ul>

                    <div class="dashboard-profile">
                        <div class="row">
                            <div class="col-md-6 col-md-push-3">
                                <form id="register-form" action="" class="form" action="" novalidate="novalidate" method="post">
                                    <ul>

                                        <li>
                                            <input type="password" name="old_password" id="old_password" placeholder="Eski Parolanız" required>
                                        </li>
                                        <li>
                                            <input type="password" name="password" id="password" placeholder="Yeni Parolanız" required>
                                        </li>
                                        <li>
                                            <input type="password" name="password-confirm" id="password-confirm" data-rule-equalto="#password" placeholder="Yeni Parolanızı Tekrarlayın" required>
                                        </li>
                                        <li>
                                            <input type="submit" value="Kaydet" class="btn btn-lg btn-secondary w100p">
                                        </li>

                                    </ul>
                                </form>
                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.body.className += " hidden-footer";
</script>
