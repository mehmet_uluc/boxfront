<?php include "shared/main_bar.php" ?>
<div class="dashboard login">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50 ">
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-8 col-md-offset-2">
                                    <h3 class="tac mb30 primary-color-color">
                                        <i class="fa fa-check-circle fz24 c-green mr10"></i>
                                        Yeni Şifre Bilgisi E-mail’ine Gönderildi.
                                    </h3>
                            </div>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="register-footer">
                                    <li>
                                        <i class="icon-free-shipping db"></i>
                                        <span> Bedava kargo</span>
                                    </li>
                                    <li>
                                        <i class="icon-cancel-account db"></i>
                                        <span> Dilediğinizde aboneliğinizi iptal etme</span>
                                    </li>
                                    <li>
                                        <i class="icon-happiness db"></i>
                                        <span>100% Müşteri memnuniyeti</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.body.className += " hidden-footer";
</script>
