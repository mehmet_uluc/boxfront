<section id="register" class="no-padding">
    <a href="" class="checkout-logo">box of us</a>
    <div class="checkout-step-wrapper">
        <div class="payment-success">
            <i class="fa fa-check-circle"></i>
            <span>Siparişiniz alınmıştır. Ödeme için çok teşekkür ederiz.</span>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50">
                    <div class="content-body">
                        <div class="row">
                            <div class="tac">
                                <div class="fz24">Sipariş Detayları</div>
                                <div class="fz24 ff-msb">Sipariş numaranız : 9920</div>
                                <div class="fz18 secondary-color">Kutularınız her ay, ayın 3-10 arasında gönderilir.</div>
                                <div class="fz15">Her kutu farklı bir konsept üzerine tasarlanılmıştır.</div>
                            </div>
                            <div class="payment-success-info">


                                <img src="assets/img/checkout-box.jpg" class="box" alt="box of us">
                               <ul class="invoice">

                                   <li>
                                       <span>1 Adet boxofus aboneliği</span>
                                       <span class="fr">100 TL</span>
                                   </li>
                                   <li>
                                       <span>ilk 3 ay promosyon indirimi</span>
                                       <span class="fr">-10 TL</span>
                                   </li>
                                   <li>
                                       <span>Kargo</span>
                                       <span class="fr">Ücretsiz</span>
                                   </li>
                                   <li>
                                       <span>Aylık ödenilecek tutar (vergiler dahil)</span>
                                       <span class="fr secondary-color">90 TL</span>
                                   </li>

                               </ul>
                            </div>
                            <div class="tac fz14 ff-msb">
                                Sipariş detaylarını, adres değişikliklerini "<a href=" ">Hesabım</a>" sayfasından takip edebilirsiniz.
                            </div>

                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="register-footer">
                                    <li>
                                        <i class="icon-free-shipping db"></i>
                                        <span> Bedava kargo</span>
                                    </li>
                                    <li>
                                        <i class="icon-cancel-account db"></i>
                                        <span> Dilediğinizde aboneliğinizi iptal etme</span>
                                    </li>
                                    <li>
                                        <i class="icon-happiness db"></i>
                                        <span>100% Müşteri memnuniyeti</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    document.body.className += " hidden-footer";
</script>