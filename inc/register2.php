<section id="register" class="no-padding">
    <a href="" class="checkout-logo">box of us</a>
    <div class="checkout-step-wrapper">
        <ul class="checkout-step">
            <li><a href=" " class="active"><span>1</span>Kayıt Ol / Üye Girişi</a></li>
            <li><a href=" " ><span>2</span>Abonelik Kutusu</a></li>
            <li><a href=" "><span>3</span>Adres Bilgileri</a></li>
            <li><a href=" "><span>4</span>Ödeme</a></li>
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50">
                    <div class="content-body">
                        <div class="row">
                            <div class="col-md-5 tac mb40">
                                <img src="assets/img/register.jpg" class="box" alt="box of us">
                            </div>
                            <div class="col-md-7">
                                <form id="register-form" class="form" action="" novalidate="novalidate" method="post">
                                    <ul class="form-wrapper">
                                        <li class="header">
                                            <span class="light-blue">boxofus'a</span> abone olmak için, <a class="secondary-color tdu" href=" ">KAYIT OL</a>
                                        </li>

                                        <li>
                                            <input name="email" id="email"  placeholder="E-mail adresin" required type="email">
                                        </li>
                                        <li>
                                            <input type="password" placeholder="Şifren" required>
                                        </li>
                                        <li class="tac">
                                            <input type="submit" class="btn btn-secondary btn-lg btn-form" value="Giriş Yap">
                                        </li>

                                        <li class="tac mt20">
                                            <a href=" " class="btn btn-lg btn-form bg-fb white"> Facebook ile Giriş Yap</a>
                                        </li>
                                        <li class="tac">
                                            <a href=" " class="fz20 ff-msb c-fb"> Şifremi unuttum</a>
                                        </li>
                                    </ul>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="register-footer">
                                    <li>
                                        <i class="icon-free-shipping db"></i>
                                        <span> Bedava kargo</span>
                                    </li>
                                    <li>
                                        <i class="icon-cancel-account db"></i>
                                        <span> Dilediğinizde aboneliğinizi iptal etme</span>
                                    </li>
                                    <li>
                                        <i class="icon-happiness db"></i>
                                        <span>100% Müşteri memnuniyeti</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    document.body.className += " hidden-footer";
</script>