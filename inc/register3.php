<section id="register" class="no-padding">
    <a href="" class="checkout-logo">box of us</a>
    <div class="checkout-step-wrapper">
        <ul class="checkout-step">
            <li><a href=" " class="success"> <span><i class="fa fa-check-circle"></i></span>Kayıt Ol / Üye Girişi</a></li>
            <li><a href=" " class="active"><span>2</span>Abonelik Kutusu</a></li>
            <li><a href=" "><span>3</span>Adres Bilgileri</a></li>
            <li><a href=" "><span>4</span>Ödeme</a></li>
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50">
                    <div class="content-body">
                        <div class="row">
                            <div class="col-md-6 tac mb40">
                                <div class="posr">
                                    <div class="discount-badge">
                                        <span>
                                            %10
                                        İNİDİRİMLİ
                                        </span>
                                    </div>
                                    <img src="assets/img/register-box.jpg" class="box" alt="box of us">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <ul class="price-table">
                                    <li>
                                        <i class="icon icon-recipe"></i>
                                        <span>
                            1 adet 2 kişilik yemek tarifi
                            ve malzemeleri
                                        </span>
                                    </li>
                                    <li>
                                        <i class="icon icon-box2"></i>
                                        <span>
                            Beraber üretebiliceğiniz
                            d.i.y. ürün malzemeleri
                        </span>
                                    </li>
                                    <li>
                                        <i class="icon icon-meet"></i>
                                        <span>
                            Birbirinizi daha iyi tanımanız
                            için hazırlanmış oyun
                            konsepti
                        </span>
                                    </li>
                                    <li>
                                        <i class="icon icon-ssl"></i>
                                        <span>
                            Güvenli Ödeme
                            SSL Sertifikası ve BDDK denetimiyle
                        </span>
                                    </li>
                                    <li>
                                        <i class="icon icon-cancel"></i>
                                        <span>
                            Dilediğinde İptal
                        </span>
                                    </li>
                                    <li>
                                        <div class="fz20 primary-color mb10">Ay’da <span class="mline">100 TL</span> yerine 90TL.</div>
                                        <div class="c-lgrey fz14">(her kutu iki kisiliktir, kutularin konsepti her ay degisir)</div>
                                    </li>
                                    <li>
                                        <a href=" " class="btn btn-lg btn-secondary w100p" > ABONE OL</a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="register-footer">
                                    <li>
                                        <i class="icon-free-shipping db"></i>
                                        <span> Bedava kargo</span>
                                    </li>
                                    <li>
                                        <i class="icon-cancel-account db"></i>
                                        <span> Dilediğinizde aboneliğinizi iptal etme</span>
                                    </li>
                                    <li>
                                        <i class="icon-happiness db"></i>
                                        <span>100% Müşteri memnuniyeti</span>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    document.body.className += " register";
</script>