<section id="register" class="no-padding">
    <a href="" class="checkout-logo">box of us</a>
    <div class="checkout-step-wrapper">
        <ul class="checkout-step">
            <li><a href=" " class="active"><span>1</span>Kayıt Ol / Üye Girişi</a></li>
            <li><a href=" "><span>2</span>Abonelik Kutusu</a></li>
            <li><a href=" "><span>3</span>Adres Bilgileri</a></li>
            <li><a href=" "><span>4</span>Ödeme</a></li>
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50">
                    <div class="content-body">
                        <div class="row">
                        <div class="col-md-5 tac mb40">
                            <img src="assets/img/register.jpg" class="box" alt="box of us">
                        </div>
                        <div class="col-md-7">
                            <form id="register-form" class="form" action="" novalidate="novalidate" method="post">
                                <ul class="form-wrapper">
                                    <li class="header">
                                        <span class="light-blue">boxofus'a</span> abone isen, <a class="secondary-color tdu" href=" ">GİRİŞ YAP</a>
                                    </li>
                                    <li>
                                        <input type="text" id="name" name="name" placeholder="Adın" minlength="2" required>
                                    </li>
                                    <li>
                                        <input type="text" id="surname" name="surname" placeholder="Soyadın" required>
                                    </li>
                                    <li>
                                        <input name="email" id="email"  placeholder="E-mail adresin" required type="email">
                                    </li>
                                    <li>
                                        <input type="password" placeholder="Şifren" required>
                                    </li>
                                    <li class="tac">
                                        <input type="submit" class="btn btn-secondary btn-lg btn-form" value="Kayıt Ol">
                                    </li>
                                    <li class="contract-row">
                                        <input name="contract" id="contract" type="checkbox" required data-msg="">
                                        <label for="contract">
                                            <a href="contract.php" target="_blank" >Üyelik sözleşmesi</a> ve

                                            <a href="contract.php" target="_blank">Gizlilik Politikasını okudum ve kabul ediyorum</a>
                                        </label>
                                    </li>
                                    <li class="contract-row">
                                        <input type="checkbox" id="campaign" name="campaign" required data-msg="">
                                        <label for="campaign">
                                            <span>Kampanya ve yeniliklerden haberdar olmak istiyorum</span>
                                        </label>
                                    </li>
                                    <li class="mt40 tac">
                                        <div class="middle-line">
                                            <span>Veya</span>
                                        </div>
                                    </li>
                                    <li class="tac mt20">
                                        <a href=" " class="btn btn-lg btn-form bg-fb white"> Facebook ile Kayıt Ol</a>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                    </div>
                    <div class="content-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="register-footer">
                                    <li>
                                        <i class="icon-free-shipping db"></i>
                                        <span> Bedava kargo</span>
                                    </li>
                                    <li>
                                        <i class="icon-cancel-account db"></i>
                                        <span> Dilediğinizde aboneliğinizi iptal etme</span>
                                    </li>
                                    <li>
                                        <i class="icon-happiness db"></i>
                                        <span>100% Müşteri memnuniyeti</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    document.body.className += " hidden-footer";
</script>

