<?php include "shared/dashboard_bar.php" ?>
<div class="dashboard">
    <div class=" container">
        <div class="row">
            <div class="col md-12">
                <div class="dashboard-inner content clearfix">
                    <ul class="dashboard-menu">
                       <li>
                           <a href=" "> Siparişlerim </a>
                       </li>
                        <li>
                            <a href=" " class="active"> Profilim</a>
                        </li>
                        <li>
                            <a href=" "> Parolam</a>
                        </li>
                        <li>
                            <a href=" "> Adreslerim</a>
                        </li>
                    </ul>

                    <div class="dashboard-profile">
                        <div class="row">
                            <div class="col-md-6 col-md-push-3">
                                <form id="register-form" action="" class="form" action="" novalidate="novalidate" method="post">
                                    <ul>
                                        <li>
                                            <select name="gender" data-rule-required="true">
                                                <option value="0">Cinsiyetiniz</option>
                                                <option value="1">Kadın</option>
                                                <option value="2">Erkek</option>
                                            </select>
                                        </li>
                                        <li>
                                            <input type="text" id="name" name="name" value="Yasemen" data-rule-required="true">
                                        </li>
                                        <li>
                                            <input type="text" id="surname" name="surname" value="Aydın" data-rule-required="true">
                                        </li>
                                        <li>
                                            <input type="email" id="email" name="email" value="yasemenaydin@gmail.com" data-rule-required="true" data-rule-email="true">
                                        </li>
                                        <li>
                                            <input type="submit" value="Kaydet" class="btn btn-lg btn-secondary w100p">
                                        </li>

                                    </ul>
                                </form>
                            </div>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.body.className += " hidden-footer";
</script>
