<?php include "shared/main_bar.php" ?>
<div class="dashboard login">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50 ">
                    <div class="content-body">
                        <div class="row">

                            <div class="col-md-8 col-md-offset-2">
                                <form id="register-form" class="form" action="" novalidate="novalidate" method="post">
                                    <h3 class="tac ff-mr mb30">boxofus Abone Girişi</h3>
                                    <ul class="form-wrapper">

                                        <li>
                                            <input name="email" id="email"  placeholder="E-mail adresin" required type="email">
                                        </li>
                                        <li>
                                            <input type="password" placeholder="Şifren" required>
                                        </li>
                                        <li class="tac">
                                            <input type="submit" class="btn btn-secondary btn-lg btn-form" value="Giriş Yap">
                                        </li>

                                        <li class="tac mt20">
                                            <a href=" " class="btn btn-lg btn-form bg-fb white"> Facebook ile Giriş Yap</a>
                                        </li>
                                        <li class="tac">
                                            <a href=" " class="fz20 ff-msb c-fb"> Şifremi unuttum</a>
                                        </li>
                                        <li class="header tac">
                                            <span class="light-blue">boxofus'a</span> abone olmak için, <a class="secondary-color tdu" href=" ">KAYIT OL</a>
                                        </li>
                                    </ul>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="register-footer">
                                    <li>
                                        <i class="icon-free-shipping db"></i>
                                        <span> Bedava kargo</span>
                                    </li>
                                    <li>
                                        <i class="icon-cancel-account db"></i>
                                        <span> Dilediğinizde aboneliğinizi iptal etme</span>
                                    </li>
                                    <li>
                                        <i class="icon-happiness db"></i>
                                        <span>100% Müşteri memnuniyeti</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.body.className += " hidden-footer";
</script>
