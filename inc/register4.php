<section id="register" class="no-padding">
    <a href="" class="checkout-logo">box of us</a>
    <div class="checkout-step-wrapper">
        <ul class="checkout-step">
            <li><a href=" " class="success"><span><i class="fa fa-check-circle"></i></span>Kayıt Ol / Üye Girişi</a></li>
            <li><a href=" " class="success"><span><i class="fa fa-check-circle"></i></span>Abonelik Kutusu</a></li>
            <li><a href=" " class="active"><span>3</span>Adres Bilgileri</a></li>
            <li><a href=" "><span>4</span>Ödeme</a></li>
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50">
                    <div class="content-body">
                        <div class="row">
                            <div class="col-md-6">
                                <form id="register-form" class="form" action="" novalidate="novalidate" method="post">
                                    <ul class="form-wrapper">
                                        <li class="header">
                                            <span class="light-blue">Kutunuzun teslim olmasını istediğiniz adres:</span>
                                        </li>
                                        <li>
                                            <input type="text" name="tag" id="tag" placeholder="Adres Etiketiniz (örn: iş, ev..)" required>
                                        </li>
                                        <li>
                                            <input type="text" id="name" name="name" placeholder="Adın" minlength="2" required>
                                        </li>
                                        <li>
                                            <input type="text" id="surname" name="surname" placeholder="Soyadın" required>
                                        </li>
                                        <li>
                                            <input type="text" id="address" name="address" placeholder="Açık Adresiniz" required>
                                        </li>
                                        <li>
                                            <select data-rule-required="true" name="city" id="city" class="btn-lg w100p" >
                                                <option value="">Şehir Seç</option>
                                                <option value="1">İstanbul</option>
                                                <option value="2">Ankara</option>
                                                <option value="3">İzmir</option>
                                            </select>
                                        </li>
                                        <li>
                                            <select data-rule-required="true" name="district" id="district" class="btn-lg w100p" >
                                                <option value="">İlçe Seç</option>
                                                <option value="1">Bakırköy</option>
                                                <option value="2">Acıbadem</option>
                                                <option value="3">Şişli</option>
                                                <option value="4">Esenler</option>
                                            </select>
                                        </li>
                                        <li class="two-column">
                                            <input type="text" id="county" name="county" placeholder="Semt" required data-msg="">

                                            <input type="text" id="post-code" name="post-code" placeholder="Posta Kodu" >
                                        </li>
                                        <li>
                                            <input type="text" id="phone" class="onlyNumeric" name="phone" placeholder="Telefon Numaranız" required maxlength="14">
                                        </li>
                                        <li>
                                            <label>
                                                <input type="checkbox"> <span class="fz18 ff-msb"> Hediye Seçeneği</span>
                                            </label>
                                        </li>
                                        <li>
                                            <input type="text" id="receiver" name="receiver" placeholder="Hediyenin Alıcısı" required>
                                        </li>
                                        <li class="tac">
                                            <input type="submit" class="btn btn-secondary btn-lg btn-form" value="Adresi Kayıt Et">
                                        </li>

                                    </ul>
                                </form>

                            </div>
                            <div class="col-md-6 tac mb40">
                                <img src="assets/img/register.jpg" class="box" alt="box of us">
                            </div>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="register-footer">
                                    <li>
                                        <i class="icon-free-shipping db"></i>
                                        <span> Bedava kargo</span>
                                    </li>
                                    <li>
                                        <i class="icon-cancel-account db"></i>
                                        <span> Dilediğinizde aboneliğinizi iptal etme</span>
                                    </li>
                                    <li>
                                        <i class="icon-happiness db"></i>
                                        <span>100% Müşteri memnuniyeti</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    document.body.className += " hidden-footer";
</script>