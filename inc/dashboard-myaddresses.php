<?php include "shared/dashboard_bar.php" ?>
<div class="dashboard">
    <div class=" container">
        <div class="row">
            <div class="col md-12">
                <div class="dashboard-inner content clearfix">
                    <ul class="dashboard-menu">
                        <li>
                            <a href=" "> Siparişlerim </a>
                        </li>
                        <li>
                            <a href=" "> Profilim</a>
                        </li>
                        <li>
                            <a href=" "> Parolam</a>
                        </li>
                        <li>
                            <a href=" " class="active"> Adreslerim</a>
                        </li>
                    </ul>

                    <div class="dashboard-myaddresses">
                        <div class="row">
                            <div class="col-md-6 col-md-push-3">
                                <form id="register-form" class="form" action="" novalidate="novalidate" method="post">
                                    <ul class="form-wrapper">

                                        <li>
                                            <input type="text" name="tag" id="tag"
                                                   placeholder="Adres Etiketiniz (örn: iş, ev..)" value="İş Adresim"
                                                   required>
                                        </li>
                                        <li>
                                            <input type="text" id="name" name="name" placeholder="Adın" value="Yasemen"
                                                   minlength="2" required>
                                        </li>
                                        <li>
                                            <input type="text" id="surname" name="surname" placeholder="Soyadın"
                                                   value="Aydın" required>
                                        </li>
                                        <li>
                                            <textarea id="address" name="address" placeholder="Açık Adresiniz">Kuyulubag sokak no:132 daire 1 sisli/ferikoy</textarea>
                                        </li>
                                        <li>
                                            <select name="city" id="city" class="btn-lg w100p"
                                                    data-rule-required="true">
                                                <option value="">Şehir Seç</option>
                                                <option value="1" selected>İstanbul</option>
                                                <option value="2">Ankara</option>
                                                <option value="3">İzmir</option>
                                            </select>
                                        </li>
                                        <li>
                                            <select name="district" id="district" class="btn-lg w100p"
                                                    data-rule-required="true">
                                                <option value="">İlçe Seç</option>
                                                <option value="1">Bakırköy</option>
                                                <option value="2">Acıbadem</option>
                                                <option value="3" selected>Şişli</option>
                                                <option value="4">Esenler</option>
                                            </select>
                                        </li>
                                        <li class="two-column">
                                            <input type="text" id="county" name="county" multiple placeholder="Semt"
                                                   value="Şişli" required data-msg="">

                                            <input type="text" id="post-code" name="post-code" value="34330"
                                                   placeholder="Posta Kodu">
                                        </li>
                                        <li>
                                            <input type="text" id="phone" class="onlyNumeric" name="phone"
                                                   value="05424006188" placeholder="Telefon Numaranız" required
                                                   maxlength="14">
                                        </li>

                                        <li class="tac">
                                            <input type="submit" class="btn btn-secondary btn-lg btn-form"
                                                   value="Adresi Kayıt Et">
                                        </li>

                                    </ul>
                                </form>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-10 mb20 col-md-offset-1">
                                <h3 class="dib m0">Kayıtlı Adreslerim:</h3>
                                <a id="add-new-address" href="javascript:void(0)" class="db tdu secondary-color fz18 ff-msb fr"> Yeni Adres Ekle</a>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="myaddress active">
                                            <div class="myaddress-header">
                                                <span>İş Adresim:</span>
                                                <span class="fr">
                                            <label class="default-address"> Varsayılan
                                                <input type="checkbox" checked>
                                            </label>

                                        </span>
                                            </div>
                                            <div class="myaddress-body">
                                                <div class="name">Yasemen Aydın</div>
                                                <div class="address">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum cumque libero
                                                    similique voluptas.
                                                </div>
                                                <div class="gsm">
                                                    05322345678
                                                </div>
                                            </div>
                                            <div class="myaddress-footer">
                                                <a class="edit-address" href="javascript:void(0)">Düzenle </a>
                                                <a class="remove-address" href="javascript:void(0)">Sil </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="myaddress">
                                            <div class="myaddress-header">
                                                <span>Ev Adresim:</span>
                                                <span class="fr">
                                            <label class="default-address"> Varsayılan
                                                <input type="checkbox">
                                            </label>

                                        </span>
                                            </div>
                                            <div class="myaddress-body">
                                                <div class="name">Yasemen Aydın</div>
                                                <div class="address">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum cumque libero
                                                    similique voluptas.
                                                </div>
                                                <div class="gsm">
                                                    05322345678
                                                </div>
                                            </div>
                                            <div class="myaddress-footer">
                                                <a class="edit-address" href="javascript:void(0)">Düzenle </a>
                                                <a class="remove-address" href="javascript:void(0)">Sil </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.body.className += " hidden-footer";
</script>
