<?php include "shared/main_bar.php" ?>

<section class="hero">
    <div class="hero-wrapper display-table">
        <div class="hero-cell display-table-cell">
            <h1 class="wow fadeInDown animated">İlişkinizi güçlendirin <br> beraber zaman geçirin </h1>
            <div class="wow fadeInUp animated">
                <a href=" " class="btn btn-lg btn-primary mr40"> ABONE OL</a>
                <a href=" " class="btn btn-lg btn-white"> HEDİYE ET</a>
            </div>
        </div>
    </div>
</section>

<section id="box-of-nedir">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="wow fadeIn animated">Date night partnerinizle rutinden uzak eğlenceli bir gece geçirmenizi sağlar. <br>
                    İlişki uzmanları tarafından hazırlanan kutular yada etkinlikler sayesinde</h2>
            </div>
        </div>
    </div>
</section>

    <section id="month">
    <div class="month-background">&nbsp;</div>
    <div class="month-wrapper display-table">
        <div class="display-table-cell">
            <div class="month-of-box wow fadeInDown animated" >
                <div class="mouth-badge wow zoomIn animated" data-wow-delay="1s" data-wow-duration=".5s"></div>
                <h2><span id="currentBox"></span> ayı box of us <br>
                    kutusunun içinde <br>
                    ne var?</h2>
                <ul>
                    <li>
                        <i class="fa fa-check"></i>
                        <span>Marshmallow ve marshmallow stickleri</span>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <span>Marshmallow mumları</span>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <span>Adventure temalı muglar</span>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <span>Tortilla yapmak için malzemeler</span>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <span>Oyun (Benimle maceraya hazır mısın?)</span>
                    </li>
                    <li>
                        <i class="fa fa-check"></i>
                        <span>... ve daha bir suru sey!</span>
                    </li>
                </ul>
                <p class="mouth-box-info">
                    Her kutu iki kişiliktir. Aylık üyelik sadece 130 TL.
                    Bu kutu örnek bir kutudur. Her ay farklı konsept'de
                    kutu tasarlanır.
                </p>
                <p class="mouth-box-cancel">
                    Üyeliğinizi istediğiniz zaman iptal edebilirsiniz.
                </p>
                <a href=" " class="btn btn-primary btn-lg">
                    <i class="icon icon-box1"> &nbsp;</i>
                    <span> Kutu Abonesi Ol</span>
                </a>
            </div>
        </div>
    </div>
</section>

<section id="work">
    <div class="work-bar">
        box of us nasıl çalışır?
    </div>
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-sm-6">
                <div class="work-box wow fadeInUp animated">
                    <i>1</i>
                    <h4>BOXOFUS ABONELİĞİ</h4>
                    <p>
                        boxofus partnerinizle rutinden uzak bir güun
                        geçirmeniz için konsept kutular hazırlar. Abone
                        olmak çok kolay olduğu gibi aboneliğinizi dilediğiniz
                        zaman iptal edebilirsiniz.
                    </p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="work-box-img">
                    <img src="assets/img/work1.jpg" alt="box of us nasıl çalışır?">
                </div>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-sm-6">
                <div class="work-box-img">
                    <img src="assets/img/work2.jpg" alt="box of us nasıl çalışır?">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="work-box wow fadeInUp animated" data-wow-delay=".2s">
                    <i>2</i>
                    <h4>boxofus KUTUN KAPIDA</h4>
                    <p>
                        boxofus partnerinizle rutinden uzak bir gün
                        ABONELER ÖZENLE HAZIRLANILMIŞ KONSEPT KUTULARI
                        HER AYIN EN GEÇ 10’UN DA KAPILARINDA BULUCAKTIR.
                        HER KUTU İKİ KİŞİ İÇİN TASARLANILMISTIR. GECENİN
                        KEYFİNİ ÇIKARIN!
                    </p>
                    </div>
            </div>
        </div>
        <div class="row no-gutter">
            <div class="col-sm-6">
                <div class="work-box wow fadeInUp animated" data-wow-delay=".3s">
                <i>3</i>
                <h4>BİRBİRİNİZİ DAHA İYİ TANIYIN</h4>
                <p>
                    İLİŞKİNİZİ DAHA SAĞLAMLAŞTIRMAK ADINA YOLA ÇIKTIK.
                    HER AY FARKLI BİR KONSEPT KUTU TASARLIYORUZ
                    SİZİN İÇİN! ŞİMDİ SIRA SİZDE, BİRBİRiNİZ İÇİN
                    ZAMAN AYIRIN VE KUTUYU DENEYİMLERKEN
                    BİRBİRİNİZİ KEŞFEDİN!
                </p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="work-box-img">
                    <img src="assets/img/work3.jpg" alt="box of us nasıl çalışır?">
                </div>
            </div>

        </div>

    </div>
</section>

<section id="price">
    <div class="price-bar">
        box of us kutu aboneliği
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <ul class="price-table">
                    <li class="wow fadeInLeft animated">
                        <i class="icon icon-recipe"></i>
                        <span>
                            1 adet 2 kişilik yemek tarifi
                            ve malzemeleri
                        </span>
                    </li>
                    <li class="wow fadeInLeft animated" data-wow-delay=".2s">
                        <i class="icon icon-box2"></i>
                        <span>
                            Beraber üretebiliceğiniz
                            d.i.y. ürün malzemeleri
                        </span>
                    </li>
                    <li class="wow fadeInLeft animated" data-wow-delay=".3s">
                        <i class="icon icon-meet"></i>
                        <span>
                            Birbirinizi daha iyi tanımanız
                            için hazırlanmış oyun
                            konsepti
                        </span>
                    </li>
                    <li class="wow fadeInLeft animated" data-wow-delay=".4s">
                        <i class="icon icon-ssl"></i>
                        <span>
                            Güvenli Ödeme
                            SSL Sertifikası ve BDDK denetimiyle
                        </span>
                    </li>
                    <li class="wow fadeInLeft animated" data-wow-delay=".5s">
                        <i class="icon icon-cancel"></i>
                        <span>
                            Dilediğinde İptal
                        </span>
                    </li>
                </ul>
                <p class="fz20 ff-mb wow fadeInLeft animated" data-wow-delay=".6s">Ay'da Sadece 130 TL.</p>
            </div>
            <div class="col-md-7 col-md-push-1 price-img-wrapper">
                <img src="assets/img/home-price.png" class="wow fadeInRight animated" data-wow-delay=".3s" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 price-buttons wow fadeInUp animated" data-wow-delay=".6s">

                    <a href=" " class="btn btn-lg btn-primary mr40"> ABONE OL</a>
                    <a href=" " class="btn btn-lg btn-white ghost"> HEDİYE ET</a>

            </div>
        </div>
    </div>
</section>

<section id="photos">
    <h2 class="photos-bar">Box of us <span>sevgililer</span> için!</h2>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-6 nopadding">
                <img src="assets/img/photo/photo1.png" alt="">
            </div>
            <div class="col-md-3 col-sm-6 nopadding">
                <img src="assets/img/photo/photo2.png" alt="">
            </div>
            <div class="col-md-3 col-sm-6 nopadding">
                <img src="assets/img/photo/photo3.png" alt="">
            </div>
            <div class="col-md-3 col-sm-6 nopadding">
                <img src="assets/img/photo/photo4.png" alt="">
            </div>
            <div class="col-md-3 col-sm-6 nopadding">
                <img src="assets/img/photo/photo5.png" alt="">
            </div>
            <div class="col-md-3 col-sm-6 nopadding">
                <img src="assets/img/photo/photo6.png" alt="">
            </div>
            <div class="col-md-3 col-sm-6 nopadding">
                <img src="assets/img/photo/photo7.png" alt="">
            </div>
            <div class="col-md-3 col-sm-6 nopadding">
                <img src="assets/img/photo/photo8.png" alt="">
            </div>
        </div>
    </div>
</section>
