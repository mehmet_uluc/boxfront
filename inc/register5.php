<section id="register" class="no-padding">
    <a href="" class="checkout-logo">box of us</a>
    <div class="checkout-step-wrapper">
        <ul class="checkout-step">
            <li><a href=" " class="success"><span><i class="fa fa-check-circle"></i></span>Kayıt Ol / Üye Girişi</a></li>
            <li><a href=" " class="success"><span><i class="fa fa-check-circle"></i></span>Abonelik Kutusu</a></li>
            <li><a href=" " class="active"><span>3</span>Adres Bilgileri</a></li>
            <li><a href=" "><span>4</span>Ödeme</a></li>
        </ul>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content mb50">
                    <div class="content-body">
                        <div class="row">
                            <form id="register-form" class="form" action="" novalidate="novalidate" method="post">
                                <div class="col-md-12 mb20">
                                    <span class="light-blue fz20 ff-msb ">Kutunuzun teslim olmasını istediğiniz adres:</span>
                                </div>
                                <div class="col-md-6">

                                        <ul class="form-wrapper">
                                            <li>
                                                <select data-rule-required="true"  name="saved-address" id="saved-address" class="btn-lg w100p">
                                                    <option value="">Adres Seç</option>
                                                    <option value="1">İş Adresim</option>
                                                    <option value="2">Ev adresim</option>
                                                </select>
                                            </li>

                                            <li>
                                                <label>
                                                    <input type="checkbox"> <span class="fz18 ff-msb"> Hediye Seçeneği</span>
                                                </label>
                                            </li>
                                            <li>
                                                <input type="text" id="receiver" name="receiver" placeholder="Hediyenin Alıcısı" required>
                                            </li>
                                        </ul>


                                </div>
                                <div class="col-md-6 mb40">
                                    <a href=" " class="tdu secondary-color fz18 ff-msb"> Yeni Adres Ekle</a>
                                </div>
                                <div class="col-md-12 tac">
                                        <input type="submit" class="btn btn-secondary btn-lg btn-form" value="Adresi Kayıt Et">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="content-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="register-footer">
                                    <li>
                                        <i class="icon-free-shipping db"></i>
                                        <span> Bedava kargo</span>
                                    </li>
                                    <li>
                                        <i class="icon-cancel-account db"></i>
                                        <span> Dilediğinizde aboneliğinizi iptal etme</span>
                                    </li>
                                    <li>
                                        <i class="icon-happiness db"></i>
                                        <span>100% Müşteri memnuniyeti</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    document.body.className += " hidden-footer";
</script>