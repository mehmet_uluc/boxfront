<?php include "shared/dashboard_bar.php" ?>
<div class="dashboard">
    <div class=" container">
        <div class="row">
            <div class="col md-12">
                <div class="dashboard-inner content clearfix">
                    <ul class="dashboard-menu">
                       <li>
                           <a href=" " class="active"> Siparişlerim </a>
                       </li>
                        <li>
                            <a href=" "> Profilim</a>
                        </li>
                        <li>
                            <a href=" "> Parolam</a>
                        </li>
                        <li>
                            <a href=" "> Adreslerim</a>
                        </li>
                    </ul>

                    <div class="dashboard-orders">
                        <p class="dashboard-orders-header">
                            <span>2 adet</span> aktif boxofus aboneliğiniz bulunmaktadır.
                        </p>

                    </div>
                    
                    <div class="box-order clearfix">
                        <div class="col-md-7 col-md-offset-5">
                            <h3>Sizin Kutunuz</h3>
                        </div>
                        <div class="col-md-4 tac">
                            <img src="assets/img/register.jpg" class="w80p" alt="">
                            <p class="box-summary">
                                Dünya’nın en tatlı ilişki kutusu.
                                Hem eğlendirir, hem sevdirir hem de
                                partnerinizle bağlarınızı güçlendirir.
                            </p>
                        </div>
                        <div class="col-md-1 hidden-xs">
                            &nbsp;
                        </div>
                        <div class="col-md-7">

                            <ul class="box-order-info">
                                <li>
                                    <span>Aboneliğe başlama tarihi:</span>
                                    <span class="value">12 Haziran 2017</span>
                                </li>
                                <li>
                                    <span>Aboneliğin bitiş tarihi:</span>
                                    <span class="value">12 Haziran 2018</span>
                                </li>
                                <li>
                                    <span>Kutunun gönderildiği adres:</span>
                                    <select name=" " id=" " class="value address-select">
                                        <option value=""> İş Adresim</option>
                                        <option value=""> Ev Adresim</option>
                                    </select>
                                </li>
                                <li>
                                    <span>Kutuların yollanıldığı tarih aralığı:</span>
                                    <span class="value">Her ayın 2-10 aralığı</span>
                                </li>
                                <li>
                                    <span>Kayıtlı kartınızdan her ay çekilen tutar:</span>
                                    <span class="value">90 TL</span>
                                </li>
                            </ul>
                            <div class="clearfix">
                                <div class="save-btn btn btn-primary btn-lg">Kaydet</div>
                            </div>
                            <div class="membership-btns clearfix">
                                <a href="javascript:void(0)" class="membership-stop"> Üyeliği Durdur</a>
                                <a href="javascript:void(0)" class="membership-continue"> Üyeliğe Devam Et</a>
                            </div>
                        </div>
                    </div>
                    <div class="box-order clearfix">
                        <div class="col-md-7 col-md-offset-5">
                            <h3>Hediye Kutunuz</h3>
                        </div>
                        <div class="col-md-4 tac">
                            <img src="assets/img/register.jpg" class="w80p" alt="">
                            <p class="box-summary">
                                Dünya’nın en tatlı ilişki kutusu.
                                Hem eğlendirir, hem sevdirir hem de
                                partnerinizle bağlarınızı güçlendirir.
                            </p>
                        </div>
                        <div class="col-md-1 hidden-xs">
                            &nbsp;
                        </div>
                        <div class="col-md-7">

                            <ul class="box-order-info">
                                <li>
                                    <span>Aboneliğe başlama tarihi:</span>
                                    <span class="value">12 Haziran 2017</span>
                                </li>
                                <li>
                                    <span>Aboneliğin bitiş tarihi:</span>
                                    <span class="value">12 Haziran 2018</span>
                                </li>
                                <li>
                                    <span>Kutunun gönderildiği adres:</span>
                                    <select name=" " id=" " class="value address-select">
                                        <option value=""> İş Adresim</option>
                                        <option value=""> Ev Adresim</option>
                                    </select>
                                </li>
                                <li>
                                    <span>Kutuların yollanıldığı tarih aralığı:</span>
                                    <span class="value">Her ayın 2-10 aralığı</span>
                                </li>
                                <li>
                                    <span>Kayıtlı kartınızdan her ay çekilen tutar:</span>
                                    <span class="value">90 TL</span>
                                </li>
                            </ul>
                            <div class="clearfix">
                                <div class="save-btn btn btn-primary btn-lg">Kaydet</div>
                            </div>
                            <div class="membership-btns clearfix">
                                <a href="javascript:void(0)" class="membership-stop"> Üyeliği Durdur</a>
                                <a href="javascript:void(0)" class="membership-continue"> Üyeliğe Devam Et</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.body.className += " hidden-footer";
</script>
