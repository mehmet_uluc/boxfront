'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify');
var spritesmith = require('gulp.spritesmith');

gulp.task('sass', function () {
    return gulp.src('assets/scss/*.scss')
        .pipe(sass({
            style:'expanded'
        }).on('error', sass.logError))
        .pipe(autoprefixer('last 5 version'))
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('prefix', function () {
    return gulp.src('assets/css/*.css')
        .pipe(prefix({
            browsers: ['last 5 version'],
            cascade: false
        }))
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('jshint', function () {
    gulp.src(['assets/js/app.js'])
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('plugins-js', function() {
    return gulp.src('assets/js/plugins/*.js')
        .pipe(concat('plugins.js',{newLine:';\n'}))
        //.pipe(uglify())
        .pipe(gulp.dest('assets/js/'));
});


gulp.task('plugins-css', function () {
    return gulp.src('assets/css/plugins/*.css')
        .pipe(concatCss("plugins.css"))
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('assets/img/sprite/*.png').pipe(spritesmith({
        imgName: 'img/sprite.png',
        cssName: 'css/sprite.css',
        algorithm: 'top-down',
        padding: 20
    }));
    return spriteData.pipe(gulp.dest('assets/'));
});
gulp.task('watch', function () {
    gulp.watch('assets/scss/*.scss', ['sass']);
    gulp.watch('assets/js/*.js', ['jshint']);
});